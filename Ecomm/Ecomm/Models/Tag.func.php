<?php

/**
 * Tag short summary.
 *
 * Tag description.
 *
 * @version 1.0
 * @author ylederre
 */

require_once 'connect.php';

function getTagsByType($articleType)
{
  global $db;

  $sql = 'SELECT * FROM TAG WHERE ID=(SELECT Fk_TAG FROM Articles WHERE Type=?)';
  
  $tag = $db->prepare($sql);
  $tag->execute(array($articleType));
  
  #while($data = $user->fetch()){
  #  return $data;
  return $tag;
  #}
}

function getTags()
{
  global $db;
  $sql = 'SELECT * FROM TAG';
  $tags = $db->query($sql);
  
  return $tags;
}

function addTag($myTag)
{
  global $db;
  $sql = 'INSERT INTO TAG SET ID=?, Name=?';
  $tag = $db->prepare($sql);
  $tag->execute(array($myTag['ID'], $myTag['Name']));
  
}
function UpdateTag($myTag, $TagID)
{
 global $db;
 $sql = 'UPDATE TAG SET Name=? WHERE ID =?';
 $tag = $db->prepare($sql);
 $tag->execute(array( $myTag['Name'],$TagID)); 
}

function DeleteTag($TagID)
{
 global $db;
 $sql = 'DELETE FROM TAG WHERE ID=?';
 $tag = $db->prepare($sql);
 $tag->execute(array($TagID)); 
}
?>

    