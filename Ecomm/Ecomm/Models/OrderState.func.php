<?php

/**
 * OrderState short summary.
 *
 * OrderState description.
 *
 * @version 1.0
 * @author ylederre
 */
 
require_once 'connect.php';
 
function GetOrderStateByOrder($OrderID)
{
  global $db;
  $sql = 'SELECT * FROM OrderState WHERE Fk_Id_Order=(SELECT ID_Order FROM Order WHERE ID=?)';
  
  $OrderState = $db->prepare($sql);
  $OrderState->execute(array($OrderID));
  
  #while($data = $user->fetch()){
  #  return $data;
  return $OrderState;
  #}
}

function AddOrderState($myOrderState)
{
  global $db;
  $sql = 'INSERT INTO OrderState SET ID=?, StateDate=?,State=?,Payment=? , Fk_Id_Order =?';
  $OrderState = $db->prepare($sql);
  $OrderState->execute(array($myOrderState['ID'], $myOrderState['StateDate'], $myOrderState['State'], $myOrderState['Payment'], $myOrderState['Fk_Id_Order']));
}

function UpdateOrderState($myOrderState, $OrderID)
{
 global $db;
 $sql = 'UPDATE OrderState SET StateDate=?,State=?,Payment=? , Fk_Id_Order =? WHERE ID =?';
 $OrderState = $db->prepare($sql);
 $OrderState->execute(array( $myOrderState['StateDate'], $myOrderState['State'], $myOrderState['Payment'], $myOrderState['Fk_Id_Order'], $OrderID)); 
}

function deleteOrderState($OrderID)
{
 global $db;
 $sql = 'DELETE FROM OrderState WHERE ID=?';
 $OrderState = $db->prepare($sql);
 $OrderState->execute(array($OrderID)); 
}
?>