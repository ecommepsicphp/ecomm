<?php

/**
 * Context short summary.
 *
 * Context description.
 *
 * @version 1.0
 * @author ylederre
 */
 Include 'Article.php';
 Include 'Comment.php';
 Include 'Img.php';
 Include 'Order.php';
 Include 'OrderState.php';
 Include 'Tag.php';
 Include 'User.php';
 Include 'Note.php';
class Context
{
    //Corriger la connection.
    public $db;
    
    
    
    public function __construct()
    {
    try{
        $this->db = new PDO('mysql:host=localhost;dbname=projet-151', 'projet-151', 'projet-151');
       }
       catch(Exception $e){
       echo "Exception re�ue : ", $e->getMessage(),"\n";
       }
    }
 
    public function GetArticles()
    {
        $article = array();
        // On admet que $db est un objet PDO
        $request = $this->db->query('SELECT ID, Name, Description, Price, Disponibility, Type, Fk_IMG, Fk_TAG, Fk_Comment FROM Articles');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $article[] = new Article($donnees['ID'],$donnees['Name'],$donnees['Description'],$donnees['Price'],$donnees['Disponibility'],$donnees['Type'],$donnees['Fk_IMG'],$donnees['Fk_TAG'],$donnees['Fk_Comment']);
        }
        
        return $article;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function GetArticle($myArticleID)
    {
        // On admet que $db est un objet PDO
        //BUGGY!!!
        //$request = $db->query('SELECT ID, Name, Description, Price, Disponibility, Type, Fk_IMG, Fk_TAG, Fk_Comment FROM Articles WHERE' . $myArticleID .'== ID');
        
        $q = $this->db->prepare('SELECT ID, Name, Description, Price, Disponibility, Type, Fk_IMG, Fk_TAG, Fk_Comment FROM Articles WHERE :myArticleID == ID');
        $q->bindValue(':myArticleID', $myArticleID);
        $q->execute();
        
        $request = $q->fetch();
        $article = new Article($request['ID'],$request['Name'],$request['Description'],$request['Price'],$request['Disponibility'],$request['Type'],$request['Fk_IMG'],$request['Fk_TAG'],$request['Fk_Comment']);
        
        return $article;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function GetArticlesByTag($myTagName)
    {
        $article = array();
        // On admet que $db est un objet PDO
        $tempReq = $this->db->query('SELECT ID FROM TAG WHERE Name == ' . $myTagName);
        $request = $this->db->query('SELECT ID, Name, Description, Price, Disponibility, Type, Fk_IMG, Fk_TAG, Fk_Comment FROM Articles WHERE Fk_TAG == ' . $tempReq);
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $article[] = new Article($donnees['ID'],$donnees['Name'],$donnees['Description'],$donnees['Price'],$donnees['Disponibility'],$donnees['Type'],$donnees['Fk_IMG'],$donnees['Fk_TAG'],$donnees['Fk_Comment']);
        }
        
        return $article;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function GetArticlesBytype($myType)
    {
        $article = array();
        // On admet que $db est un objet PDO
        
        $request = $this->db->query('SELECT ID, Name, Description, Price, Disponibility, Type, Fk_IMG, Fk_TAG, Fk_Comment FROM Articles WHERE Type == ' . $myType);
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $article[] = new Article($donnees['ID'],$donnees['Name'],$donnees['Description'],$donnees['Price'],$donnees['Disponibility'],$donnees['Type'],$donnees['Fk_IMG'],$donnees['Fk_TAG'],$donnees['Fk_Comment']);
        }
        
        return $article;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function AddArticles($myArticle)
    {
        $q = $this->db->prepare('INSERT INTO Article SET Name = :name, Description = :description, Price = :price, Disponibility = :disponibility, Type = :type, Fk_IMG = :fk_IMG, Fk_TAG= :fk_TAG, Fk_Comment= :fk_Comment ');

        $q->bindValue(':name', $myArticle->$name);
        $q->bindValue(':description', $myArticle->$description);
        $q->bindValue(':price', $myArticle->$price);
        $q->bindValue(':disponibility', $myArticle->$disponibility);
        $q->bindValue(':type', $myArticle->$type);
        $q->bindValue(':fk_IMG', $myArticle->$fk_IMG);
        $q->bindValue(':fk_TAG', $myArticle->$fk_TAG);
        $q->bindValue(':fk_Comment', $myArticle->$fk_Comment);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function UpdateArticles($myArticle, $myArticleID)
    {
        $q = $this->db->prepare('UPDATE Article SET Name = :name, Description = :description, Price = :price, Disponibility = :disponibility, Type = :type, Fk_IMG = :fk_IMG, Fk_TAG= :fk_TAG, Fk_Comment= :fk_Comment WHERE ID ==' . $myArticleID );

        $q->bindValue(':name', $myArticle->$name, PDO::PARAM_INT);
        $q->bindValue(':description', $myArticle->$description, PDO::PARAM_INT);
        $q->bindValue(':price', $myArticle->$price, PDO::PARAM_INT);
        $q->bindValue(':disponibility', $myArticle->$disponibility, PDO::PARAM_INT);
        $q->bindValue(':type', $myArticle->$type, PDO::PARAM_INT);
        $q->bindValue(':fk_IMG', $myArticle->$fk_IMG, PDO::PARAM_INT);
        $q->bindValue(':fk_TAG', $myArticle->$fk_TAG, PDO::PARAM_INT);
        $q->bindValue(':fk_Comment', $myArticle->$fk_Comment, PDO::PARAM_INT);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function deleteArticles($myArticleID)
    {
      $this->db->exec('DELETE FROM Articles WHERE ID = '.$myArticleID->id());
      //fermeture de la connexion PDO
      $db = null;
    }
    
    public function GetComments()
    {
        $comment = array();
        // On admet que $db est un objet PDO
        $request = $this->db->query('SELECT ID, Contents, Display, Datetime FROM Comment');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $comment[] = new Comment($donnees['ID'],$donnees['Contents'],$donnees['Display'],$donnees['Datetime']);
        }      
        return $comment;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function GetCommentsByArticle($articleid)
    {
        $comment = array();
        // On admet que $db est un objet PDO
        $tempReq = $this->db->query('SELECT Fk_Comment FROM Articles WHERE ID == ' . $articleid);
        $request = $this->db->query('SELECT ID, Contents, Display, Datetime FROM Comment WHERE' . $tempReq . ' == ID');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $comment[] = new Comment($donnees['ID'],$donnees['Contents'],$donnees['Display'],$donnees['Datetime']);
        }      
        return $comment;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function AddComment($myComment)
    {
        $q = $this->$db->prepare('INSERT INTO Comment SET Contents = :contents, Display = :display, Datetime = :datetime');

        $q->bindValue(':contents', $myComment->$contents);
        $q->bindValue(':display', $myComment->$display);
        $q->bindValue(':datetime', $myComment->$datetime);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function UpdateComment($myComment, $myCommentID)
    {
        $q = $this->$db->prepare('UPDATE Comment SET Contents = :contents, Display = :display, Datetime = :datetime WHERE ID ==' . $myCommentID);

        $q->bindValue(':contents', $myComment->$contents, PDO::PARAM_INT);
        $q->bindValue(':display', $myComment->$display, PDO::PARAM_INT);
        $q->bindValue(':datetime', $myComment->$datetime, PDO::PARAM_INT);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function deleteComment($myCommentID)
    {
      $this->db->exec('DELETE FROM Comment WHERE ID = '.$myCommentID->id());
      //fermeture de la connexion PDO
      $db = null;
    }
    
    public function GetImgs()
    {
        $img = array();
        // On admet que $db est un objet PDO
        $request = $this->db->query('SELECT ID, URL FROM IMG');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $img[] = new Img($donnees['ID'],$donnees['URL']);
        }      
        return $img;
        //fermeture de la connexion PDO
        $db = null;
    }
        
    public function GetImgsByArticle($imgid)
    {
        $img = array();
        // On admet que $db est un objet PDO
        $tempReq = $this->db->query('SELECT Fk_IMG FROM Articles WHERE ID == ' . $imgid);
        $request = $this->db->query('SELECT ID, URL FROM IMG WHERE' . $tempReq . ' == ID');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $img[] = new Img($donnees['ID'],$donnees['URL']);
        }      
        return $img;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function AddImg($myImg)
    {
        $q = $this->db->prepare('INSERT INTO IMG SET URL = :URL');

        $q->bindValue(':URL', $myImg->$url);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function UpdateImg($myImg, $myImgID)
    {
        $q = $this->db->prepare('UPDATE IMG SET URL = :URL WHERE ID ==' . $myImgID);

        $q->bindValue(':URL', $myImg->$url, PDO::PARAM_INT);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function deleteImg($myImgID)
    {
      $this->db->exec('DELETE FROM IMG WHERE ID = '.$myImgID->id());
      //fermeture de la connexion PDO
      $db = null;
    }
    
    public function GetOrders()
    {
        $order = array();
        // On admet que $db est un objet PDO
        $request = $this->db->query('SELECT ID, ID_Order,Order_Date,Quantity,Fk_Articles FROM Order');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $order[] = new Order($donnees['ID'],$donnees['ID_Order'],$donnees['Order_Date'],$donnees['Quantity'],$donnees['Fk_Articles']);
        }      
        return $order;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function GetOrdersByUser($userid)
    {
        $order = array();
        // On admet que $db est un objet PDO
        $tempReq = $this->db->query('SELECT Fk_Order FROM Users WHERE ID == ' . $userid);
        $request = $this->db->query('SELECT ID, ID_Order,Order_Date,Quantity,Fk_Articles FROM Order WHERE' . $tempReq . '== ID');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
	  $order[] = new Order($donnees['ID'],$donnees['ID_Order'],$donnees['Order_Date'],$donnees['Quantity'],$donnees['Fk_Articles']);
        }      
        return $order;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function AddOrder($myOrder)
    {
        $q = $this->db->prepare('INSERT INTO Order SET ID_Order = :ID_Order, Order_Date = :Order_Date, Quantity = :quantity, Fk_Articles = :fk_Articles');

        $q->bindValue(':ID_Order', $myOrder->$id_order);
        $q->bindValue(':Order_Date', $myOrder->$order_date);
        $q->bindValue(':quantity', $myOrder->$quantity);
        $q->bindValue(':fk_Articles', $myOrder->$fk_article);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function UpdateOrder($myOrder, $myOrderID)
    {
      $q = $this->db->prepare('UPDATE Order SET ID_Order = :ID_Order, Order_Date = :Order_Date, Quantity = :quantity, Fk_Articles = :fk_Articles WHERE ID ==' . $myOrderID);

        $q->bindValue(':ID_Order', $myOrder->$id_order, PDO::PARAM_INT);
	$q->bindValue(':Order_Date', $myOrder->$order_date, PDO::PARAM_INT);
        $q->bindValue(':quantity', $myOrder->$quantity, PDO::PARAM_INT);
        $q->bindValue(':fk_Articles', $myOrder->$fk_article, PDO::PARAM_INT);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function deleteOrder($myOrderID)
    {
      $this->db->exec('DELETE FROM Order WHERE ID = '.$myOrderID->id());
      //fermeture de la connexion PDO
      $db = null;
    }
    
    public function GetTags()
    {
        $tag = array();
        // On admet que $db est un objet PDO
        $request = $this->db->query('SELECT ID, Name FROM TAG');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $tag[] = new Tag($donnees['ID'],$donnees['Name']);
        }      
        return $tag;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function GetTagsByType($type)
    {
        $tag = array();
        // On admet que $db est un objet PDO
        $tempReq = $this->db->query('SELECT Fk_TAG FROM Articles WHERE Type == ' . $type);
        $request = $this->db->query('SELECT ID, Name FROM TAG WHERE' . $tempReq . '== ID');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $tag[] = new Tag($donnees['ID'],$donnees['Name']);
        }      
        return $tag;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function AddTag($myTag)
    {
        $q = $this->db->prepare('INSERT INTO TAG SET Name = :name');

        $q->bindValue(':name', $myTag->$name);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function UpdateTag($myTag, $myTagID)
    {
        $q = $this->db->prepare('UPDATE TAG SET Name = :name WHERE ID ==' . $myTagID);

        $q->bindValue(':name', $myTag->$name, PDO::PARAM_INT);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function deleteTag($myTagID)
    {
      $this->db->exec('DELETE FROM TAG WHERE ID = '.$myTagID->id());
      //fermeture de la connexion PDO
      $db = null;
    }
    
    public function GetUsers()
    {
        $user = array();
        // On admet que $db est un objet PDO
        $request = $this->db->query('SELECT ID,Role,Adress,Name,Firstname,birthdate,Email,Password,country,city,Fk_Order,Fk_Comment FROM Users');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $user[] = new User($donnees['ID'],$donnees['Role'],$donnees['Adress'],$donnees['Name'],$donnees['Firstname'],$donnees['birthdate'],$donnees['Email'],$donnees['Password'],$donnees['country'],$donnees['city'],$donnees['Fk_Order'],$donnees['Fk_Comment']);
        }      
        return $user;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function GetUser($myUserEmail)
    {
        // On admet que $db est un objet PDO
        $request = $this->db->query('SELECT ID,Role,Adress,Name,Firstname,birthdate,Email,Password,country,city,Fk_Order,Fk_Comment FROM Users WHERE' . $myUserEmail . ' == Email');
        
        $user = new User($donnees['ID'],$donnees['Role'],$donnees['Adress'],$donnees['Name'],$donnees['Firstname'],$donnees['birthdate'],$donnees['Email'],$donnees['Password'],$donnees['country'],$donnees['city'],$donnees['Fk_Order'],$donnees['Fk_Comment']);   
        return $user;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function AddUser($myUser)
    {
        $q = $this->db->prepare('INSERT INTO Users SET Role = :role, Adress = :adress, Name = :name, Firstname = :firstname, birthdate = :birthdate, Email = :email, Password= :password, country= :country, city = :city, Fk_Order = :fk_Order, Fk_Comment = :fk_Comment ');

        $q->bindValue(':role', $myUser->Role);
        $q->bindValue(':adress', $myUser->Address);
        $q->bindValue(':name', $myUser->Name);
        $q->bindValue(':firstname', $myUser->Firstname);
        $q->bindValue(':birthdate', $myUser->Birthdate);
        $q->bindValue(':email', $myUser->Email);
        $q->bindValue(':password', $myUser->Password);
	$q->bindValue(':country', $myUser->Country);
	$q->bindValue(':city', $myUser->City);
	$q->bindValue(':fk_Order', $myUser->Fk_order);
	$q->bindValue(':fk_Comment', $myUser->Fk_comment);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function UpdateUser($myUser, $myUserID)
    {
        $q = $this->db->prepare('UPDATE Users SET Role = :role, Adress = :adress, Name = :name, Firstname = :firstname, birthdate = :birthdate, Email = :email, Password= :password, country= :country, city = :city, Fk_Order = :fk_Order, Fk_Comment = :fk_Comment WHERE ID ==' . $myUserID);

        $q->bindValue(':role', $myUser->$role, PDO::PARAM_INT);
        $q->bindValue(':adress', $myUser->$address, PDO::PARAM_INT);
        $q->bindValue(':name', $myUser->$name, PDO::PARAM_INT);
        $q->bindValue(':firstname', $myUser->$firstname, PDO::PARAM_INT);
        $q->bindValue(':birthdate', $myUser->$birthdate, PDO::PARAM_INT);
        $q->bindValue(':email', $myUser->$email, PDO::PARAM_INT);
        $q->bindValue(':password', $myUser->$password, PDO::PARAM_INT);
        $q->bindValue(':country', $myUser->$country, PDO::PARAM_INT);
        $q->bindValue(':city', $myUser->$city, PDO::PARAM_INT);
        $q->bindValue(':fk_Order', $myUser->$fk_order, PDO::PARAM_INT);
        $q->bindValue(':fk_Comment', $myUser->$fk_comment, PDO::PARAM_INT);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function deleteUser($myUserID)
    {
      $this->db->exec('DELETE FROM Users WHERE ID = '.$myUserID->id());
      //fermeture de la connexion PDO
      $db = null;
    }
    
    public function GetNotes()
    {
        $note = array();
        // On admet que $db est un objet PDO
        $request = $this->db->query('SELECT ID,Note,Fk_Articles FROM Note');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $note[] = new Note($donnees['ID'],$donnees['Note'],$donnees['Fk_Articles']);
        }      
        return $note;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function GetNotesByArticle($article)
    {
        $note = array();
        // On admet que $db est un objet PDO
        $tempReq = $this->db->query('SELECT ID FROM Articles WHERE Name == ' . $article);
        $request = $this->db->query('SELECT ID, Note FROM Note WHERE' . $tempReq . '== Fk_Articles');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $note[] = new Note($donnees['ID'],$donnees['Note'],$donnees['Fk_Articles']);
        }      
        return $note;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function AddNote($myNote)
    {
        $q = $this->db->prepare('INSERT INTO Note SET Note = :note,Fk_Articles = :fk_Articles ');

        $q->bindValue(':note', $myNote->$note);
        $q->bindValue(':fk_Articles', $myNote->$fk_article);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function UpdateNote($myNote, $myNoteID)
    {
        $q = $this->db->prepare('UPDATE Note SET Note = :note,Fk_Articles = :fk_Articles WHERE ID ==' . $myNoteID);

        $q->bindValue(':note', $myNote->$note, PDO::PARAM_INT);
        $q->bindValue(':fk_Articles', $myNote->$fk_article, PDO::PARAM_INT);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function deleteNote($myNoteID)
    {
      $this->db->exec('DELETE FROM Note WHERE ID = '.$myNoteID->id());
      //fermeture de la connexion PDO
      $db = null;
    }
    
    public function GetOrderStateByOrder($myOrderID)
    {
        $orderstate = array();
        // On admet que $db est un objet PDO
        $tempReq = $this->db->query('SELECT ID_Order FROM Order WHERE ID == ' . $myOrderID);
        $request = $this->db->query('SELECT ID, StateDate, State, Payment, Fk_Id_Order FROM OrderState WHERE' . $tempReq . '== Fk_Id_Order');
        
        while ($donnees = $request->fetch(PDO::FETCH_ASSOC))
        {
            $orderstate[] = new OrderState($donnees['ID'],$donnees['StateDate'],$donnees['State'],$donnees['Payment'],$donnees['Fk_Id_Order']);
        }      
        return $orderstate;
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function AddOrderState($myOrderState)
    {
        $q = $this->db->prepare('INSERT INTO OrderState SET StateDate = :statedate,State = :state, Payment = :payment, Fk_Id_Order = :fk_Id_Order  ');

        $q->bindValue(':statedate', $myOrderState->$statedate);
        $q->bindValue(':state', $myOrderState->$state);
        $q->bindValue(':payment', $myOrderState->$payment);
        $q->bindValue(':fk_Id_Order', $myOrderState->$fk_orderstate);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function UpdateOrderState($myOrderState, $myOrderStateID)
    {
        $q = $this->db->prepare('UPDATE OrderState SET StateDate = :statedate,State = :state, Payment = :payment, Fk_Id_Order = :fk_Id_Order WHERE ID ==' . $myOrderStateID);

        $q->bindValue(':statedate', $myOrderState->$statedate, PDO::PARAM_INT);
        $q->bindValue(':state', $myOrderState->$state, PDO::PARAM_INT);
        $q->bindValue(':payment', $myOrderState->$payment, PDO::PARAM_INT);
        $q->bindValue(':fk_Id_Order', $myOrderState->$fk_orderstate, PDO::PARAM_INT);

        $q->execute();
        //fermeture de la connexion PDO
        $db = null;
    }
    
    public function deleteOrderState($myOrderStateID)
    {
      $this->db->exec('DELETE FROM OrderState WHERE ID = '.$myOrderStateID->id());
      //fermeture de la connexion PDO
      $db = null;
    }
}
?>
