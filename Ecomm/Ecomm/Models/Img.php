<?php

/**
 * Img short summary.
 *
 * Img description.
 *
 * @version 1.0
 * @author ylederre
 */
class Img
{
    private $id = 0;
    private $url = "www.google.ch";
    
    public function __construct($Id=NULL, $Url=NULL) 
    { 
        $this->id = $Id;
        $this->url = $Url;
    }
    
    public function showImg()
    {
    $myImg = array();
      
      $myImg['Id'] = $this->id;
      $myImg['Url'] = $this->url;
      
      return $myImg;
      }
    
      public function GetImgs()
    {
     $sql = 'SELECT ID, URL FROM IMG';
     
     $imgs = $this->executeReq($sql);
     return $imgs;
    }
    
    //public function GetImgsByArticle($imgid)
    //{
    //  $sql = 'SELECT ID, URL FROM IMG WHERE ID=?';
      
    //  $img = $this->executeReq($sql, array($imgid));
    //  return $img;      
    //}
    
    public function AddImg($myImg)
    {
    $sql = 'INSERT INTO IMG SET URL = :URL';
    
     $q->bindValue(':URL', $myImg->url);
    
    $this->executeReq($sql, array($q));
    }
    
    public function UpdateImg($myImg, $myImgID)
    {
    $sql = 'UPDATE IMG SET URL = :URL WHERE ID =?';

        $q->bindValue(':URL', $myImg->url, PDO::PARAM_INT);

        $this->executeReq($sql, array($myImgID,$q));
    }
    
    public function deleteImg($myImgID)
    {
     $sql =  'DELETE FROM IMG WHERE ID =?';
     
     $this->executeReq($sql, array($myImgID));   
    }
    
      
}

//Exemple Utilisation du constructeur.
//$Img_exemple = new Img(1,"www.facebook.com");
?>
