<?php

class Model
{
    private $db;


    private $id=0;
    //LE RÔLE  = 1 user, 2= Admin article, 3=Admin User
    private $role=1;
    private $name="toto";
    private $firstname="Toto";
    private $birthdate;
    private $email="toto@gmail.com";
    private $password="password";
  private $country="suisse";
  private $city="lausanne";
  private $address="Sur rosset 24 A";
  //Tableau contenant la liste des commande et commentaire : correspondant aux ID de la table listée.
  public $fk_order = array(1,2,3,4,5);
  public $fk_comment = array(2,3,5);
  public $fk_note = array(4,1,2);
  
  public function __construct($Id=NULL, $Role=NULL, $Name=NULL, $Firstname=NULL, $Birthdate=NULL, $Email=NULL, $Password=NULL, $Country=NULL, $City=NULL, $Address=NULL, $Fk_Order=NULL, $Fk_Comment=NULL, $Fk_Note=NULL) 
  {
    $this->id = $Id;
    $this->role = $Role;
    $this->name = $Name; 
    $this->firstname = $Firstname;
    $this->birthdate = date('d/m/Y h:i:s a');
    $this->birthdate = $Birthdate;
    $this->email = $Email;
    $this->password = $Password; 
  $this->country = $Country;
  $this->city = $City; 
  $this->address = $Address;
  $this->fk_order = $Fk_Order; 
  $this->fk_comment = $Fk_Comment;
  $this->fk_note = $Fk_Note;
  
  }

    private function getDB() {
        if ($this->db == null) {
	  $this->db = new PDO('mysql:host=sql.lautre.net;dbname=illambias_ecomm', 'illambias_ecomm', 'ecomm', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
	
	return $this->db;
    }
	
    protected function executeReq($sql, $params = null) {
	if ($params == null) {
	   $result = $this->getDB()->query($sql);
	   $result->setFetchMode(PDO::FETCH_CLASS, 'User');
	}
	else {
	  $result = $this->getDB()->prepare($sql);
	  $result->execute($params);
	  $result->setFetchMode(PDO::FETCH_CLASS, 'User');
	}
	return $resultat;
    }

    //protected function executeReqAdd($sql, $params){
    //}
}



?>
