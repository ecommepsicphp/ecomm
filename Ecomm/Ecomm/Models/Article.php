<?php

/**
 * Article short summary.
 *
 * Article description.
 *
 * @version 1.0
 * @author ylederre
 */
class Article
{
    private $id = 0;
    private $name = "Toto";
    private $description="Cet article est un article d'exemple";
    private $price=00.00;
    private $disponiblity=True;
    private $type="v�tement";
    //Tableau contenant la liste des img / tag / commentaire : correspondant aux ID de la table list�e.
    public $fk_img = array(1,2,3,4,5);
    public $fk_tag = array(1,2);
    public $fk_comment = array(1,2);
    
    public function __construct($Id=NULL, $Name=NULL, $Descritpion=NULL, $Price=NULL, $Disponibility=NULL, $Type=NULL, $FK_Img=NULL, $FK_Tag=NULL, $FK_Comment=NULL) 
    { 
        $this->id = $Id; 
        $this->name = $Name;
        $this->description = $Descritpion;
        $this->price = $Price; 
        $this->disponiblity = $Disponibility; 
        $this->type = $Type; 
        $this->fk_img = $FK_Img;
        $this->fk_tag = $FK_Tag;
        $this->fk_comment = $FK_Comment;
    }
    
    public function showArticle()
    {
    $myArticle = array();
      
      $myArticle['Id'] = $this->id;
      $myArticle['Name'] = $this->name;
      $myArticle['Descritpion'] = $this->description;
      $myArticle['Price'] = $this->price;
      $myArticle['Disponibility'] = $this->disponiblity;
      $myArticle['Type'] = $this->type;
      $myArticle['FK_Img'] = $this->fk_img;
      $myArticle['FK_Tag'] = $this->fk_tag;
      $myArticle['FK_Comment'] = $this->fk_comment;
      
      return $myArticle;
      }
      
    public function getArticle($myArticleID)
    {
      $sql = 'SELECT ID, Name, Description, Price, Disponibility, Type, Fk_IMG, Fk_TAG, Fk_Comment FROM Articles WHERE ID=?';
      
      $article = $this->executeReq($sql, array($myArticleID));
      return $article;      
    }
    
    public function getArticles()
    {
     $sql = 'SELECT ID, Name, Description, Price, Disponibility, Type, Fk_IMG, Fk_TAG, Fk_Comment FROM Articles';
     
     $articles = $this->executeReq($sql);
     return $articles;
    }
    
    //public function getArticleByTag($myTagName)
    //{
    //  $sql = 'SELECT ID, Name, Description, Price, Disponibility, Type, Fk_IMG, Fk_TAG, Fk_Comment FROM Articles WHERE ID=?';
      
    //  $article = $this->executeReq($sql, array($myTagName));
    //  return $article;      
    //}
    
    //public function getArticleByType($myTypeName)
    //{
    //  $sql = 'SELECT ID, Name, Description, Price, Disponibility, Type, Fk_IMG, Fk_TAG, Fk_Comment FROM Articles WHERE ID=?';
      
    //  $article = $this->executeReq($sql, array($myTagName));
    //  return $article;      
    //}
    
    public function AddArticle($myArticle)
    {
    $sql = 'INSERT INTO Article SET Name = :name, Description = :description, Price = :price, Disponibility = :disponibility, Type = :type, Fk_IMG = :fk_IMG, Fk_TAG= :fk_TAG, Fk_Comment= :fk_Comment ';
    
     $q->bindValue(':name', $myArticle->name);
        $q->bindValue(':description', $myArticle->description);
        $q->bindValue(':price', $myArticle->price);
        $q->bindValue(':disponibility', $myArticle->disponibility);
        $q->bindValue(':type', $myArticle->type);
        $q->bindValue(':fk_IMG', $myArticle->fk_IMG);
        $q->bindValue(':fk_TAG', $myArticle->fk_TAG);
        $q->bindValue(':fk_Comment', $myArticle->fk_Comment);
    
    $this->executeReq($sql, array($q));
    }
    
    public function UpdateUser($myArticle, $myArticleID)
    {
    $sql = 'UPDATE Article SET Name = :name, Description = :description, Price = :price, Disponibility = :disponibility, Type = :type, Fk_IMG = :fk_IMG, Fk_TAG= :fk_TAG, Fk_Comment= :fk_Comment WHERE ID =?';

        $q->bindValue(':name', $myArticle->name, PDO::PARAM_INT);
        $q->bindValue(':description', $myArticle->description, PDO::PARAM_INT);
        $q->bindValue(':price', $myArticle->price, PDO::PARAM_INT);
        $q->bindValue(':disponibility', $myArticle->disponibility, PDO::PARAM_INT);
        $q->bindValue(':type', $myArticle->type, PDO::PARAM_INT);
        $q->bindValue(':fk_IMG', $myArticle->fk_IMG, PDO::PARAM_INT);
        $q->bindValue(':fk_TAG', $myArticle->fk_TAG, PDO::PARAM_INT);
        $q->bindValue(':fk_Comment', $myArticle->fk_Comment, PDO::PARAM_INT);

        $this->executeReq($sql, array($myArticleID,$q));
    }
    
    public function deleteArticles($myArticleID)
    {
     $sql =  'DELETE FROM Articles WHERE ID=?';
     
     $this->executeReq($sql, array($myArticleID));   
    }
}



//Exemple Utilisation du constructeur.
//$article_exemple = new Article(1,"marc","tu pu du cul",23.90,true,"v�tement",array(3,5,2),array(35,32,4),array(23,14,53));
?>
