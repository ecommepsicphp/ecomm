<?php

/**
 * Note short summary.
 *
 * Note description.
 *
 * @version 1.0
 * @author ylederre
 */
 
require_once 'connect.php';
 
function GetNotesByArticle($articleName)
{
  global $db;
  $sql = 'SELECT * FROM Note WHERE Fk_Articles=(SELECT ID FROM Articles WHERE Name=?)';
  
  $Note = $db->prepare($sql);
  $Note->execute(array($articleName));
  
  #while($data = $user->fetch()){
  #  return $data;
  return $Note;
  #}
}

function GetNotes()
{
  global $db;
  $sql = 'SELECT * FROM Note';
  $Notes = $db->query($sql);
  
  return $Notes;
}

function AddNote($myNote)
{
  global $db;
  $sql = 'INSERT INTO Note SET ID=?, Note=?, Fk_Articles=?';
  $Note = $db->prepare($sql);
  $Note->execute(array($myNote['ID'], $myNote['Note'], $myNote['Fk_Articles'])); 
}
function UpdateNote($myNote, $myNoteID)
{
 global $db;
 $sql = 'UPDATE Note SET Note=?, Fk_Articles=? WHERE ID=?';
 $Note = $db->prepare($sql);
 $Note->execute(array($myNote['Note'], $myNote['Fk_Articles'], $myNoteID)); 
}

function DeleteNote($myNoteID)
{
 global $db;
 $sql = 'DELETE FROM Note WHERE ID=?';
 $Note = $db->prepare($sql);
 $Note->execute(array($myNoteID)); 
} 
?>