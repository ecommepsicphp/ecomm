<?php

/**
 * User short summary.
 *
 * User description.
 *
 * @version 1.0
 * @author ylederre
 */
 
require_once 'Model.php';

class User extends Model
{
    
    private $id=0;
    //LE R�LE  = 1 user, 2= Admin article, 3=Admin User
    private $role=1;
    private $name="toto";
    private $firstname="Toto";
    private $birthdate;
    private $email="toto@gmail.com";
    private $password="password";
    private $country="suisse";
    private $city="lausanne";
    private $address="Sur rosset 24 A";
    //Tableau contenant la liste des commande et commentaire : correspondant aux ID de la table list�e.
    public $fk_order = array(1,2,3,4,5);
    public $fk_comment = array(2,3,5);
    public $fk_note = array(4,1,2);
    
    public function __construct($Id=NULL, $Role=NULL, $Name=NULL, $Firstname=NULL, $Birthdate=NULL, $Email=NULL, $Password=NULL, $Country=NULL, $City=NULL, $Address=NULL, $Fk_Order=NULL, $Fk_Comment=NULL, $Fk_Note=NULL) 
    {
        $this->id = $Id;
        $this->role = $Role;
        $this->name = $Name; 
        $this->firstname = $firstname;
        $this->birthdate = date('d/m/Y h:i:s a');
        $this->birthdate = $Birthdate;
        $this->email = $Email;
        $this->password = $Password; 
        $this->country = $Country;
        $this->city = $City; 
        $this->address = $Address;
        $this->fk_order = $Fk_Order; 
        $this->fk_comment = $Fk_Comment;
        $this->fk_note = $Fk_Note;
        
    }
    
    public function showUser()
    {
      $myUser = array();
      
      $myUser['Id'] = $this->id;
      $myUser['Role'] = $this->role;
      $myUser['Name'] = $this->name;
      $myUser['Firstname'] = $this->firstname;
      $myUser['Birthdate'] = $this->birthdate;
      $myUser['Email'] = $this->email;
      $myUser['Password'] = $this->password;
      $myUser['Country'] = $this->country;
      $myUser['City'] = $this->city;
      $myUser['Address'] = $this->address;
     

      
      return $myUser;
    }
    
    public function getUser($myUserEmail)
    {
      $sql = 'SELECT ID,Role,Adress,Name,Firstname,birthdate,Email,Password,country,city,Fk_Order,Fk_Comment FROM Users WHERE Email=?';
      
      $user = $this->executeReq($sql, array($myUserEmail));
      return $user;
      
    }
    
    
    
}
//Exemple Utilisation du constructeur.
//$user_exemple = new User(2,1,"L�che","Moi",date_create(string $time=now),"l�che-moi@gmail.com","password","suisse","lausanne","Sur rosset 24a",2,3,2);
?>