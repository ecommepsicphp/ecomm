<?php

/**
 * Order short summary.
 *
 * Order description.
 *
 * @version 1.0
 * @author ylederre
 */

require_once 'connect.php';s

function GetOrdersByUser($userid)
{
  global $db;  
  $sql = 'SELECT * FROM Order WHERE ID=(SELECT Fk_Order FROM Users WHERE ID=?)';
  
  $Order = $db->prepare($sql);
  $Order->execute(array($userid));
  
  #while($data = $user->fetch()){
  #  return $data;
  return $Order;
  #}
}

function GetOrders()
{
  global $db;
  $sql = 'SELECT * FROM Order';
  $Orders = $db->query($sql);
  
  return $Orders;
}

function AddOrder($myOrder)
{
  global $db;
  $sql = 'INSERT INTO Order SET ID=?, ID_Order=?, Order_Date=?, Quantity=?, Fk_Articles=?';
  $Order = $db->prepare($sql);
  $Order->execute(array($myOrder['ID'], $myOrder['ID_Order'], $myOrder['Order_Date'], $myOrder['Quantity'], $myOrder['Fk_Articles']));
  
}
function UpdateOrder($myOrder, $OrderID)
{
 global $db;
 $sql = 'UPDATE INTO SET ID_Order=?, Order_Date=?, Quantity=?, Fk_Articles=? WHERE ID=?';
 $Order = $db->prepare($sql);
 $Order->execute(array($myOrder['ID_Order'], $myOrder['Order_Date'], $myOrder['Quantity'], $myOrder['Fk_Articles'], $OrderID)); 
}

function DeleteOrder($OrderID)
{
 global $db;
 $sql = 'DELETE FROM Order WHERE ID=?';
 $Order = $db->prepare($sql);
 $Order->execute(array($OrderID)); 
}
?>