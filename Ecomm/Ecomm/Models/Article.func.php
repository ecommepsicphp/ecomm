<?php

/**
 * Article short summary.
 *
 * Article description.
 *
 * @version 1.0
 * @author ylederre
 */
 require_once 'connect.php';
 
function GetArticlesByTag($TagName)
{
  global $db;     
  $sql = 'SELECT * FROM Articles WHERE Fk_TAG=(SELECT ID FROM TAG WHERE Name=?)';
  
  $Article = $db->prepare($sql);
  $Article->execute(array($TagName));
  
  #while($data = $user->fetch()){
  #  return $data;
  return $Article;
  #}
}

function GetArticlesBytype($Type)
{
  global $db;
  
  $sql = 'SELECT * FROM Articles WHERE Type=? ';
  
  $Articles = $db->prepare($sql);
  $Articles->execute(array($Type));
  
  #while($data = $user->fetch()){
  #  return $data;
  return $Articles;
  #}
}

function GetArticles()
{
  global $db;
  $sql = 'SELECT * FROM Articles';
  $Articles = $db->query($sql);
  
  return $Articles;
}

function GetArticle($ArticleID)
{
  global $db;
  
  $sql = 'SELECT * FROM Articles WHERE ID=?';
  
  $Article = $db->prepare($sql);
  $Article->execute(array($ArticleID));
  
  #while($data = $user->fetch()){
  #  return $data;
  return $Article;
  #}
}

function AddArticles($myArticle)
{
  global $db;
  $sql = 'INSERT INTO Articles SET ID=?, Name=?, Description=?, Price=?, Disponibility=?, Type=?, Fk_IMG=?, Fk_TAG=?, Fk_Comment=? ';
  $Article = $db->prepare($sql);
  $Article->execute(array($myArticle['ID'], $myArticle['Name'], $myArticle['Description'], $myArticle['Price'], $myArticle['Disponibility'], $myArticle['Type'], $myArticle['Fk_IMG'], $myArticle['Fk_TAG'], $myArticle['Fk_Comment'])); 
}
function UpdateArticles($myArticle, $ArticleID)
{
 global $db;
 $sql = 'UPDATE Articles Name=?, Description=?, Price=?, Disponibility=?, Type=?, Fk_IMG=?, Fk_TAG=?, Fk_Comment=? WHERE ID=?';
 $Article = $db->prepare($sql);
 $Article->execute(array($myArticle['Name'], $myArticle['Description'], $myArticle['Price'], $myArticle['Disponibility'], $myArticle['Type'], $myArticle['Fk_IMG'], $myArticle['Fk_TAG'], $myArticle['Fk_Comment'], $ArticleID));  
}

function DeleteArticles($ArticleID)
{
 global $db;
 $sql = 'DELETE FROM Articles WHERE ID=?';
 $Article = $db->prepare($sql);
 $Article->execute(array($ArticleID)); 
}  
?>       
