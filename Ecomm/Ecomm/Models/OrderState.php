<?php

/**
 * OrderState short summary.
 *
 * OrderState description.
 *
 * @version 1.0
 * @author ylederre
 */
class OrderState
{
    private $id=0;
    private $statedate;
    //le state est de 1 � 3 // en cours / valid� / envoy�
    private $state = 1;
    //le payment // facture / carte de cr�dit
    private $payment = 1;
    public $fk_idorder = array(1,2,3,4,5);
    
    public function __construct($Id=NULL, $Statedate=NULL, $State=NULL,$Payment=NULL, $fk_IdOrder=NULL) 
    { 
         $this->id = $Id;
         $this->statedate = date('d/m/Y h:i:s a');
         $this->statedate = $Statedate;
         $this->state = $State;  
         $this->payment = $Payment; 
         $this->fk_idorder = $fk_IdOrder;   
    }
    
    public function showOrderState()
    {
    $myOrderState = array();
      
      $myOrderState['Id'] = $this->id;
      $myOrderState['Statedate'] = $this->statedate;
      $myOrderState['State'] = $this->state;
      $myOrderState['Payment'] = $this->payment;
      $myOrderState['fk_IdOrder'] = $this->fk_idorder;
      
      return $myOrderState;
      }
}