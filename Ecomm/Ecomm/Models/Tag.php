<?php

/**
 * Tag short summary.
 *
 * Tag description.
 *
 * @version 1.0
 * @author ylederre
 */
class Tag
{
    private $id=0;
    private $name="t-SHIRT";
    
    public function __construct($Id=NULL, $Name=NULL) 
    {
        $this->id = $Id;
        $this->name = $Name;      
    }
    
    public function showTag()
    {
    $myTag = array();
      
      $myTag['Id'] = $this->id;
      $myTag['Name'] = $this->name;
      
      return $myTag;
      }
}
//Exemple Utilisation du constructeur.
//$tag_exemple = new Tag(1,"montag");
?>
