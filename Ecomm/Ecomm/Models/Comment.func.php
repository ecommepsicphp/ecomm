<?php

/**
 * Comment short summary.
 *
 * Comment description.
 *
 * @version 1.0
 * @author ylederre
 */
 
 
 require_once 'connect.php';
 
function GetCommentsByArticle($articleid)
{
  global $db;  
  $sql = 'SELECT * FROM Comment WHERE ID=(SELECT Fk_Comment FROM Articles WHERE ID=?)';
  
  $Comment = $db->prepare($sql);
  $Comment->execute(array($articleid));
  
  #while($data = $user->fetch()){
  #  return $data;
  return $Comment;
  #}
}

function GetComments()
{
  global $db;
  $sql = 'SELECT * FROM Comment';
  $Comments = $db->query($sql);
  
  return $Comments;
}

function AddComment($myComment)
{
  global $db;
  $sql = 'INSERT INTO Comment SET ID=?, Contents=?, Display=?, Datetime=?';
  $Comment = $db->prepare($sql);
  $Comment->execute(array($myComment['ID'], $myComment['Contents'], $myComment['Display'], $myComment['Datetime'])); 
}
function UpdateComment($myComment, $CommentID)
{
 global $db;
 $sql = 'UPDATE Comment SET Contents=?, Display=?, Datetime=? WHERE ID=?';
 $Comment = $db->prepare($sql);
 $Comment->execute(array($myComment['Contents'], $myComment['Display'], $myComment['Datetime'], $CommentID));  
}

function DeleteComment($myCommentID)
{
 global $db;
 $sql = 'DELETE FROM Comment WHERE ID=?';
 $Comment = $db->prepare($sql);
 $Comment->execute(array($myCommentID)); 
} 
?>

