<?php

/**
 * Commande short summary.
 *
 * Commande description.
 *
 * @version 1.0
 * @author ylederre
 */
class Order
{
    private $id=0;
    //Num�ro de commande en plus de l'ID
    private $id_order="ab24nfh34";
    private $order_date;
    private $quantity=0;  
    //Tableau contenant la liste des img / tag / commentaire : correspondant aux ID de la table list�e.
    public $fk_article = array(1,2,3,4,5);
    
    public function __construct($Id=NULL, $Id_Order=NULL, $Quantity=NULL, $Order_Date=NULL, $Fk_Article=NULL) 
    {
        $this->id = $Id;
        $this->id_order = $Id_Order;
        $this->order_date = date('d/m/Y h:i:s a');
        $this->order_date = $Order_Date;
        $this->quantity = $Quantity;
        $this->fk_article = $Fk_Article;
        
    }
    
    public function showOrder()
    {
    $myOrder = array();
      
      $myOrder['Id'] = $this->id;
      $myOrder['Id_Order'] = $this->id_order;
      $myOrder['Order_Date'] = $this->order_date;
      $myOrder['Quantity'] = $this->quantity;
      $myOrder['Fk_Article'] = $this->fk_article;
      
      return $myOrder;
      }
}
//Exemple Utilisation du constructeur.
//$Order_exemple = new Order(2,"ab23fg00",date_create(string $time=now),2,3);
?>
