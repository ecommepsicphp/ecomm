<?php

/**
 * Note short summary.
 *
 * Note description.
 *
 * @version 1.0
 * @author ylederre
 */
class Note
{
    private $id=0;
    private $note=1;
    public $fk_article = array(1,2,3,4,5);
    
    public function __construct($Id=NULL, $Note=NULL, $Fk_Article=NULL) 
    { 
        $this->id = $Id; 
        $this->note = $Note;
        $this->fk_article = $FK_Article;  
    }
    
    public function showNote()
    {
    $myNote = array();
      
      $myNote['Id'] = $this->id;
      $myNote['Note'] = $this->note;
      $myNote['FK_Article'] = $this->fk_article;
      
      return $myNote;
      }
      
      public function GetNotes()
    {
     $sql = 'SELECT ID,Note,Fk_Articles FROM Note';
     
     $notes = $this->executeReq($sql);
     return $notes;
    }
    
    //public function GetNotesByArticle($articleid)
    //{
    //  $sql = 'SELECT ID, Note FROM Note WHERE ID=?';
      
    //  $note = $this->executeReq($sql, array($articleid));
    //  return $note;      
    //}
    
    public function AddNote($myNote)
    {
    $sql = 'INSERT INTO Note SET Note = :note,Fk_Articles = :fk_Articles ';
    
     $q->bindValue(':note', $myNote->note);
        $q->bindValue(':fk_Articles', $myNote->fk_article);
    
    $this->executeReq($sql, array($q));
    }
    
    public function UpdateNote($myNote, $myNoteID)
    {
    $sql = 'UPDATE Note SET Note = :note,Fk_Articles = :fk_Articles WHERE ID =?';

        Value(':note', $myNote->note, PDO::PARAM_INT);
        $q->bindValue(':fk_Articles', $myNote->fk_article, PDO::PARAM_INT);

        $this->executeReq($sql, array($myNoteID,$q));
    }
    
    public function deleteNote($myNoteID)
    {
     $sql =  'DELETE FROM Note WHERE ID =?';
     
     $this->executeReq($sql, array($myNoteID));   
    }
}
