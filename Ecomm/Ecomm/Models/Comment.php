<?php

/**
 * Commentaire short summary.
 *
 * Commentaire description.
 *
 * @version 1.0
 * @author ylederre
 */
class Comment
{
    private $id= 0;
    private $contents="mon commentaire";
    private $display = true;
    private $datetime;

    public function __construct($Id=NULL, $Contents=NULL, $Display=NULL, $Datetime=NULL) 
    { 
        $this->id = $Id;
        $this->contents = $Contents;
        $this->display = $Display;
        $this->datetime = date("d/m/Y h:i:s a");
        $this->datetime = $Datetime;
    }
    
    public function showComment()
    {
    $myComment = array();
      
      $myComment['Id'] = $this->id;
      $myComment['Contents'] = $this->contents;
      $myComment['Display'] = $this->display;
      $myComment['Datetime'] = $this->datetime;
      
      return $myComment;
      }
    
    public function GetComments()
    {
     $sql = 'SELECT ID, Contents, Display, Datetime FROM Comment';
     
     $comments = $this->executeReq($sql);
     return $comments;
    }
    
    //public function GetCommentsByArticle($articleid)
    //{
    //  $sql = 'SELECT ID, Contents, Display, Datetime FROM Comment WHERE ID=?';
      
    //  $comment = $this->executeReq($sql, array($articleid));
    //  return $comment;      
    //}
    
    public function AddComment($myComment)
    {
    $sql = 'INSERT INTO Comment SET Contents = :contents, Display = :display, Datetime = :datetime';
    
     $q->bindValue(':contents', $myComment->contents);
        $q->bindValue(':display', $myComment->display);
        $q->bindValue(':datetime', $myComment->datetime);
    
    $this->executeReq($sql, array($q));
    }
    
    public function UpdateComment($myComment, $myCommentID)
    {
    $sql = 'UPDATE Comment SET Contents = :contents, Display = :display, Datetime = :datetime WHERE ID =?';

        $q->bindValue(':contents', $myComment->contents, PDO::PARAM_INT);
        $q->bindValue(':display', $myComment->display, PDO::PARAM_INT);
        $q->bindValue(':datetime', $myComment->datetime, PDO::PARAM_INT);

        $this->executeReq($sql, array($myCommentID,$q));
    }
    
    public function deleteComment($myCommentID)
    {
     $sql =  'DELETE FROM Comment WHERE ID =?';
     
     $this->executeReq($sql, array($myCommentID));   
    }
}
    
#Exemple Utilisation du constructeur.
#$comment_exemple = new Comment(1,"voici mon commentaire",true,date_create(string $time=now))
?>
