<?php
 
 Include 'Models/User.func.php';
 Include 'Models/Article.func.php';
 Include 'Models/Comment.func.php';
 Include 'Models/Img.func.php';
 Include 'Models/Note.func.php';
 Include 'Models/Order.func.php';
 Include 'Models/OrderState.func.php';
 Include 'Models/Tag.func.php';
 
 echo "<h3> Hello World! voici les test de models</h3>";
 echo '<br />';
 echo '<br />';
 echo '<br />';
 
 ///////////////////////////
 //instancier un new context
 ///////////////////////////

 #$test = new Context;


  
 #aller chercher un user en particulier
$user_test = getUser("issey.llambias@camptocamp.com");
  
foreach($user_test as $user)
{
echo "Affichage d'un user where email = issey.llambias@camptocamp.com"."<br />";
echo '<br />';
echo $user['Role'] . " avec le nom " . $user['Name'];
echo '<br />';
echo $user['Email'];
echo '<br />';
echo $user['Birthdate'];
echo "</br ></br >";
}

#aller chercher tous les users
$user_test2 = getUsers();
 
 echo "Affichage de tous les Users"."<br />";
 
foreach($user_test2 as $user)
{
echo '<br />';
echo $user['Role'] . " avec le nom " . $user['Name'];
echo '<br />';
echo $user['Email'];
echo '<br />';
echo $user['Birthdate'];
echo "</br ></br >";
}

#ajouter un user:
$newUser[] = array();
echo "Creation d'un user";
$newUser['ID'] = '';
$newUser['Role'] = 2;
$newUser['Adress'] = 'Rue Centrale 31';
$newUser['Name'] = 'Dumat';
$newUser['Firstname'] = 'Dupond';
$newUser['birthdate'] = '31/06/1989';
$newUser['Email'] = 'Dumat2@test.test';
$newUser['Password'] = '1234hehe';
$newUser['country'] = 'Syldavie';
$newUser['city'] = 'UneVille';
$newUser['Fk_Order'] = 45;
$newUser['Fk_Comment'] = '';

addUser($newUser);
echo "Creation du user terminée !";
#afficher l'utilisateur ajouté avant:

$user_test3 = getUser("Dumat2@test.test");
  
foreach($user_test3 as $user)
{
echo "Affichage d'un user where email = Dumat2@test.test"."<br />";
echo '<br />';
echo $user['ID'];
echo '<br />';
echo $user['Role'] . " avec le nom " . $user['Name'];
echo '<br />';
echo $user['Email'];
echo '<br />';
echo $user['Birthdate'];
echo "</br ></br >";
}
//Delete du user qui a l'ID 20'
DeleteUser(20);

# Test de l'update:
$Emailupdate = "Dumat2@test.test";
$newUserupdate[] = array();
echo "Creation d'un user temp pour le update";
$newUserupdate['Role'] = 2;
$newUserupdate['Adress'] = 'Rue Centrale 31';
$newUserupdate['Name'] = 'update';
$newUserupdate['Firstname'] = 'update';
$newUserupdate['birthdate'] = '31/06/1989';
$newUserupdate['Email'] = 'update@test.test';
$newUserupdate['Password'] = '1234hehe';
$newUserupdate['country'] = 'Syldavie';
$newUserupdate['city'] = 'UneVille';
$newUserupdate['Fk_Order'] = 34;
$newUserupdate['Fk_Comment'] = '';

UpdateUser($newUserupdate, $Emailupdate);

$user_test4 = getUser("update@test.test");
  
foreach($user_test4 as $user)
{
echo "Affichage d'un user where email = update@test.test"."<br />";
echo '<br />';
echo $user['ID'];
echo '<br />';
echo $user['Role'] . " avec le nom " . $user['Name'];
echo '<br />';
echo $user['Email'];
echo '<br />';
echo $user['Birthdate'];
echo "</br ></br >";
}
?>
