<?php
	#################################################################
	#
	#	Fichier :	veriflogin.php
	#	Auteurs :	Lederet Yann, Llambias Issey, Monthoux Caroline
	#
	#################################################################
	#
	# 	Date :		Janvier 2015
	#	Version :	1.0
	#
	#################################################################
	#
	#	D�pendances : connexion.php et users.csv
	#	But du fichier : V�rification du login et mot de passe
	#
	#################################################################
	
	// Identificateurs
	$users_path='C:/wamp/www/Module/users/';		// Dossier du fichier utilisateurs
	$users_file='users.csv';						// Fichier des utilisateurs
	
	$users=array();									// Liste des utilisateurs pour authentification
	
	// Fonction de v�rification du couple Utilisateur et mot de passe
		function verification($nom, $pass, &$data)
		{
			$verify = false;
		
			for ($x=0; $x < count($data); $x++)
			{
				if ( ($data[$x][0] == $nom) && ($data[$x][1] == md5($pass)) )
				{
					$verify = true;
					break;
				}
			}
		
			return ($verify);
		}

	// Chargement des donn�es UTILISATEURS et MOT DE PASSE depuis fichier CSV
		// Le dossier existe-t-il ?
		if( is_dir($users_path) )
		{
			// Ouverture du dossier
			if( $path = opendir($users_path) )
			{
				// Lecture des fichiers pr�sents
				while( ($file = readdir($path)) != FALSE )
				{
					// Est-ce un fichier de donn�es ?
					if( $file != '.' && $file != '..' )
					{
						// Est-ce le bon fichier de donn�es ?
						if ( $file == $users_file )
						{
							// Ouverture du fichier en lecture
							$handle = fopen($users_path.$file, "r");
						
							// Titres des colonnes (Lecture et ocultation de la premi�re ligne du fichier CSV)
							fgetcsv($handle, 1000, ";");
						
							// Tableau des donn�es utilisateurs
							$index = 0;
						
							// M�morisation des donn�es (Couple UTILISATEUR et MOT DE PASSE)
							while ( ($data = fgetcsv($handle, 1000, ";")) !== FALSE )
							{    
								// R�cup�re les donn�es dans un tableau � deux dimensions (uniquement 1�re et deuxi�me colonne)
								foreach ($data as $key => $value)
								{
									$users[$index][$key]=$value;
								}
							
								// Incr�mente compteur donn�e suivante
								$index++;
							}
											
							// Fermeture du fichier
							fclose($handle);
						}
						else
						{
							// Message d'avertissement
							echo 'Le fichier des donn�es n\'existe pas ou n\'est pas disponible pour l\'instant...<br/>';
						}
					}
				}
			}
	
			// Fermeture du dossier
			closedir($path);
		}
		else
		{
			// Message d'avertissement
			echo 'Le dossier des donn�es n\'existe pas ou n\'est pas disponible pour l\'instant...<br/>';
		}
?>