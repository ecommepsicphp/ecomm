﻿<?php
	#################################################################
	#
	#	Fichier :	header.php
	#	Auteurs :	Lederet Yann, Llambias Issey, Monthoux Caroline
	#
	#################################################################
	#
	# 	Date :		Janvier 2015
	#	Version :	1.0
	#
	#################################################################
	#
	#	Dépendances : 
	#	But du fichier : En-tête du site
	#
	#################################################################
?>

<html>
	<head>
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
	</head>
		
	<div class="header">
		<div class="bottom-header">
			<div class="container">
				<div class="header-bottom-left">
					<div class="logo">
						<a href="index.php"><img src="images/store1.png" alt=" " /></a>
					</div>
					<div class="search">
						<input type="text" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}" >
						<input type="submit"  value="Rechercher">

					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="header-bottom-right">
					<ul class="men-grid">
						<li>
							<?php
								if ( isset($_SESSION['name']) ) 
								{  ?>
									<a href="compte.php"><?php echo 'Mon compte';
								}
								else
								{	?>
									<a href="connexion.php"><?php echo 'Mon compte';
								}
							?>
						</li>
						<li>
							<?php
								if (isset($_SESSION['name']) )
								{	?>
									<a href="deconnexion.php"><?php echo 'Déconnexion';
								}
								else
								{	?>
									<a href="connexion.php"><?php echo 'Connexion';
								}
							?>
						</li>	
						<li><a href="inscription.php">Inscription</a></li>
						<li class="cart"><a href="panier.php">Mon panier</a></li>
					</ul>
				</div>
				<div class="clearfix"> </div>	
			</div>
		</div>
	</div>
</html>