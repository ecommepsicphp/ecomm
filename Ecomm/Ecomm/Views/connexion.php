﻿<?php
	#################################################################
	#
	#	Fichier :	connexion.php
	#	Auteurs :	Lederet Yann, Llambias Issey, Monthoux Caroline
	#
	#################################################################
	#
	# 	Date :		Janvier 2015
	#	Version :	1.0
	#
	#################################################################
	#
	#	Dépendances : 
	#	But du fichier : page de login
	#
	#################################################################
	
	session_start();

// Identificateurs	
	$pseudo='';					// Nom du dernier utilisateur
	$message_erreur='';			// Message d'erreur
	
/* Le nom du dernier utilisateur existe-t-il dans le cookie ?
	if ( isset($_COOKIE['UserName'] ) )
	{
		$pseudo=$_COOKIE['UserName'];
	}
	*/	
	
	if ( isset($_POST['submit']) )
	{
		// Authentification - vérification des données UTILISATEUR et MOT DE PASSE
		include("veriflogin.php");
	
		// Les données du formulaire sont-elles complÃ¨tes ?
		if (  $_POST['user']!='' &&  $_POST['password']!=''  )
		{ 
			// OUI
			// On mémorise les informations
			$user = $_POST['user'] ; 
			$pwd = $_POST['password'];
	
			//  Le couple Nom et mot de passe sont-ils valides ?
			if ( verification($user, $pwd, $users ) )
			{ 
				// OUI
				// L'utilisateur est identifiÃ© 
				// et on change d'identifiant de session 
				session_regenerate_id();
		
				// On mémorise son nom
				$_SESSION['name']=$user;

				/* Mémorisation de l'utilisateur est-elle souhaitée?
				if ( isset($_POST['memo']) )
				{
					if ($_REQUEST['memo']=='yes')
					{
						// OUI
						// Pseudo mémorisé dans un cookie
						setcookie('UserName',$user,time()+3600);
					}
					else
					{
						// NON
						// Cookie vidé
						setcookie('UserName');
					}
				}
				
				*/
				// Retour à la page principale
				header('location:index.php');
				exit();
			}
			else
			{
				// NON
				// Utilisateur non valide ! 
				$message_erreur = '<br/>Le pseudo est inconnu ou le mot de passe n\'est pas valide! <br/><br/><br/>' ; 
			}
		}
		else
		{
			// NON
			// Message d'avertissement
			$message_erreur ='<br/>Veuillez complÃ©ter tous les champs ! <br/><br/><br/>'; 
		}
	}
	
?>
<html>
<head>
<title>Connexion | Rogeiro Store</title>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
</head>
<body> 
	<!--Header-->
	<?php
	include('header.php');
	?>
	
	<!--Body-->
	<div class="container">
		<div class="account_grid">
			<div class=" login-right">
				<h3>Déjà client ?</h3>
				<p>Si vous avez déjà un compte chez nous, veuillez vous identifier.</p>
				
				<!-- Formulaire de connexion -->
				<form action="connexion.php" method="post">
					<?php
					//Message éventuel d'erreur de saisie des données
					if ( $message_erreur != '')
					{
						echo'<p><font color="ff0000">'.$message_erreur.'<font/><font color="000000"><font/></p>';
					}
					?>
					<div>
						<span>Adresse e-mail<label>*</label></span>
						<input type="text" name="user" /> 
					</div>
					<div>
						<span>Mot de passe<label>*</label></span>
						<input type="password" name="password" /> 
					</div>
					<input type="submit" name="submit" value="Login">
					<a class="forgot" href="#">Mot de passe oublié ?</a>
				</form>
			</div>	
			<div class=" login-left">
				<h3>Nouveau client</h3>
				<p>En créant un compte chez nous, vous aurez la possibilité de commander plus facilement, d'enregistrer plusieurs adresses de livraison, de suivre vos commandes... et bien plus !</p>
				<a class="acount-btn" href="register.php">Créer un compte</a>
			</div>
			<div class="clearfix"> </div>
		</div>
			<!--Catégories d'articles-->
			<div class="sub-cate">
				<div class=" top-nav rsidebar span_1_of_left">
					<h3 class="cate">Catégories</h3>
					<ul class="menu">
						<li class="item1"><a href="#">Personnalités<img class="arrow-img" src="images/arrow1.png" alt=""/> </a>
							<ul class="cute">
								<li class="subitem"><a href="liste_articles.php">Angelo Rogeiro </a></li>
								<li class="subitem"><a href="liste_articles.php">Olivier Maccaud </a></li>
							</ul>
						</li>
						<li>
							<ul class="kid-menu">
								<li><a href="liste_articles.php">Peluches</a></li>
								<li><a href="liste_articles.php">Déguisements</a></li>
								<li><a href="liste_articles.php">Nourriture</a></li>
								<li><a href="liste_articles.php">Goodies</a></li>
								<li><a href="liste_articles.php">Beauté</a></li>
								<li><a href="liste_articles.php">Geek</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<!--initiate accordion-->
				<script type="text/javascript">
					$(function() {
						var menu_ul = $('.menu > li > ul'),
						menu_a  = $('.menu > li > a');
					menu_ul.hide();
					menu_a.click(function(e) {
						e.preventDefault();
						if(!$(this).hasClass('active')) {
							menu_a.removeClass('active');
							menu_ul.filter(':visible').slideUp('normal');
							$(this).addClass('active').next().stop(true,true).slideDown('normal');
						} else {
							$(this).removeClass('active');
							$(this).next().stop(true,true).slideUp('normal');
						}
					});
			
				});
				</script>
			</div>
		<div class="clearfix"> </div>
	</div>
	
	<!--Footer-->
	<?php
	include('footer.php');
	?>
</body>
</html>