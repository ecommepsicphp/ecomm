﻿<?php
	session_start();
?>

<html>
	<head>
		<title>Nom article | Rogeiro Store</title>
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
		<link rel="stylesheet" href="css/etalage.css" type="text/css" media="all" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.easydropdown.js"></script>
		<script src="js/jquery.etalage.min.js"></script>
		<script>
			jQuery(document).ready(function($){

				$('#etalage').etalage({
					thumb_image_width: 300,
					thumb_image_height: 400,
					source_image_width: 900,
					source_image_height: 1200,
					show_hint: true,
					click_callback: function(image_anchor, instance_id){
						alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
					}
				});

			});
		</script>

	</head>

<!--Header-->
	<?php
		include('header.php');
	?>
<!--Body-->
	<body>

	
	 <div class="container">  	
	 	<div class=" single_top">
	      <div class="single_grid">
			<div class="grid images_3_of_2">
						<ul id="etalage">
							<li>
								<a href="optionallink.html">
									<img class="etalage_thumb_image" src="images/data1.jpg" class="img-responsive" />
									<img class="etalage_source_image" src="images/data.jpg" class="img-responsive" title="" />
								</a>
							</li>
							<li>
								<img class="etalage_thumb_image" src="images/data1.jpg" class="img-responsive" />
								<img class="etalage_source_image" src="images/data.jpg" class="img-responsive" title="" />
							</li>
							<li>
								<img class="etalage_thumb_image" src="images/data1.jpg" class="img-responsive"  />
								<img class="etalage_source_image" src="images/data.jpg"class="img-responsive"  />
							</li>
						    <li>
								<img class="etalage_thumb_image" src="images/data1.jpg" class="img-responsive"  />
								<img class="etalage_source_image" src="images/data.jpg"class="img-responsive"  />
							</li>
						</ul>
						 <div class="clearfix"> </div>		
				  </div> 
				  <div class="desc1 span_3_of_2">
				  	<ul class="back">
                	  <li><i class="back_arrow"> </i>Retour à <a href="liste_articles.php">la liste d'articles</a></li>
                    </ul>
					<h1>TITRE ARTICLE</h1>
					<ul class="price_single">
					  <li class="head"><h2>PRIX ARTICLE</h2></li>
					  <div class="clearfix"> </div>
					</ul>
					<p>DESCRIPTION ARTICLE</p>
				    
			         <a href="#" class="now-get">Ajouter au panier</a>
			        
				</div>
          	    <div class="clearfix"> </div>
          	   </div>
          	   <ul id="flexiselDemo1">
			<li><img src="images/angelo.png" /><div class="grid-flex"><a href="article.php">ARTICLE 1</a><p>PRIX</p></div></li>
			<li><img src="images/angelo.png" /><div class="grid-flex"><a href="article.php">ARTICLE 2</a><p>PRIX</p></div></li>
			<li><img src="images/angelo.png" /><div class="grid-flex"><a href="article.php">ARTICLE 3</a><p>PRIX</p></div></li>
			<li><img src="images/angelo.png" /><div class="grid-flex"><a href="article.php">ARTICLE 4</a><p>PRIX</p></div></li>
			<li><img src="images/angelo.png" /><div class="grid-flex"><a href="article.php">ARTICLE 5</a><p>PRIX</p></div></li>
		 </ul>
	    <script type="text/javascript">
		 $(window).load(function() {
			$("#flexiselDemo1").flexisel({
				visibleItems: 5,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
		    	responsiveBreakpoints: { 
		    		portrait: { 
		    			changePoint:480,
		    			visibleItems: 1
		    		}, 
		    		landscape: { 
		    			changePoint:640,
		    			visibleItems: 2
		    		},
		    		tablet: { 
		    			changePoint:768,
		    			visibleItems: 3
		    		}
		    	}
		    });
		    
		});
	</script>
	<script type="text/javascript" src="js/jquery.flexisel.js"></script>
	
          	   </div>
          	   
          	<!--Catégories d'articles-->
			<div class="sub-cate">
				<div class=" top-nav rsidebar span_1_of_left">
					<h3 class="cate">Catégories</h3>
					<ul class="menu">
						<li class="item1"><a href="#">Personnalités<img class="arrow-img" src="images/arrow1.png" alt=""/> </a>
							<ul class="cute">
								<li class="subitem"><a href="liste_articles.php">Angelo Rogeiro </a></li>
								<li class="subitem"><a href="liste_articles.php">Olivier Maccaud </a></li>
							</ul>
						</li>
						<li>
							<ul class="kid-menu">
								<li><a href="liste_articles.php">Peluches</a></li>
								<li><a href="liste_articles.php">Déguisements</a></li>
								<li><a href="liste_articles.php">Nourriture</a></li>
								<li><a href="liste_articles.php">Goodies</a></li>
								<li><a href="liste_articles.php">Beauté</a></li>
								<li><a href="liste_articles.php">Geek</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<!--initiate accordion-->
				<script type="text/javascript">
					$(function() {
						var menu_ul = $('.menu > li > ul'),
						menu_a  = $('.menu > li > a');
					menu_ul.hide();
					menu_a.click(function(e) {
						e.preventDefault();
						if(!$(this).hasClass('active')) {
							menu_a.removeClass('active');
							menu_ul.filter(':visible').slideUp('normal');
							$(this).addClass('active').next().stop(true,true).slideDown('normal');
						} else {
							$(this).removeClass('active');
							$(this).next().stop(true,true).slideUp('normal');
						}
					});
			
				});
				</script>
			</div>
		<div class="clearfix"> </div>
	</div>
<div class="clearfix"> </div>			
		</div>
		
	<!--Footer-->
	<?php
		include('footer.php');
	?>
</body>
</html>