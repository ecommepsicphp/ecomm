﻿<?php
	#################################################################
	#
	#	Fichier :	footer.php
	#	Auteurs :	Lederet Yann, Llambias Issey, Monthoux Caroline
	#
	#################################################################
	#
	# 	Date :		Janvier 2015
	#	Version :	1.0
	#
	#################################################################
	#
	#	Dépendances : 
	#	But du fichier : Pied-de-page du site
	#
	#################################################################
?>

<html>
	<head>
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
	</head>

	<div class="footer">
		<div class="footer-top">
			<div class="container">
				<div class="latter">
					<h6>NEWSLETTER</h6>
					<div class="sub-left-right">
						<form>
							<input type="text" value="Votre e-mail ici"onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Votre e-mail ici';}" />
							<input type="submit" value="INSCRIPTION" />
						</form>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
				<div class="footer-bottom-cate">
					<h6>Contact</h6>
					<ul>
						<li>Clients privés</li>
						<li>info@rogeiroshop.ch</li>
						<li>Lundi - Vendredi</li>
						<li>09:00 - 11:00, 14:00 - 16:00</li>
				</div>
				<div class="footer-bottom-cate">
					<h6>Emplois</h6>
					<ul>
						<li>02.01.2015</li>
						<li>Technicien de surface</li>
						<li> </li>
						<li>11.01.2015</li>
						<li>Vendeur en magasin</li>
					</ul>
				</div>
				<div class="footer-bottom-cate bottom-grid-cat">
					<h6> </h6>
				</div>
				<div class="footer-bottom-cate cate-bottom">
					<h6>Nous trouver</h6>
					<ul>
						<li>Rogeiro Shop</li>
						<li>Route d'Eclagnens 5</li>
						<li>1376 Goumoëns</li>
						<li class="phone">Tél : +41 21 709 27 31</li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
</html>