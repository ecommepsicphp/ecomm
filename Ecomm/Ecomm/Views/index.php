<?php
	#################################################################
	#
	#	Fichier :	index.php
	#	Auteurs :	Lederet Yann, Llambias Issey, Monthoux Caroline
	#
	#################################################################
	#
	# 	Date :		Janvier 2015
	#	Version :	1.0
	#
	#################################################################
	#
	#	Dépendances : 
	#	But du fichier : index du site
	#
	#################################################################

	session_start();
?>

<html>
	<head>
		<title>Rogeiro Store</title>
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="shortcut icon" href="favicon.ico">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.easydropdown.js"></script>

	</head>

	<!--Header-->
	<?php
	include('header.php');
	?>
	
	<!--Body-->
	<div class="container">
		<div class="banner-menu">		
			<div class="shoes-grid">
				<div class="wmuSlider example1 slide-grid">
					<div class="wmuSliderWrapper">
						<article style="position: absolute; width: 100%; opacity: 0;">
					   	<div class="banner-matter">
							<img class="img-responsive banner-bag" src="images/lapin3.png" alt=" " />
							<div class="banner-off">
								<h2>BÉNÉFICIEZ</h2>
								<span>D'ARTICLES DE <b>QUALITÉ</b></span>
								<p>Nos produits sont fabriqués mains par des experts en la matière. Swiss-made garanti ! </p>
								<a class="now-get" href="liste_articles.php">JE VEUX !</a>
							</div>
							<div class="clearfix"> </div>
						</div>
					 	</article>
					 	<article style="position: absolute; width: 100%; opacity: 0;">
						<div class="banner-matter">
							<img class="img-responsive banner-bag" src="images/lapin1.jpg" alt=" " />
							<div class="banner-off">
								<h2>PROFITEZ</h2>
								<span>D'UNE OFFRE <b>UNIQUE</b></span>
								<p>En collaboration avec les meilleurs paparazzis, nous travaillons d'arrache-pied pour vous plaire. En toute légalité ! </p>
								<a class="now-get" href="liste_articles.php">JE VEUX !</a>
							</div>
							<div class="clearfix"> </div>
						</div>
					 	</article>
					 	<article style="position: absolute; width: 100%; opacity: 0;">
						<div class="banner-matter">
							<img class="img-responsive banner-bag" src="images/lapin2.jpg" alt=" " />
							<div class="banner-off">
								<h2>JOUISSEZ</h2>
								<span>D'UN STATUT <b>PRIVILÉGIÉ</b></span>
								<p>Grâce à nos produits, tous vos amis vont mourir de jalousie. Vous serez inégalable ! </p>
								<a class="now-get" href="liste_articles.php">JE VEUX !</a>
							</div>
							<div class="clearfix"> </div>
						</div>
					 	</article>
					 </div>
	                <ul class="wmuSliderPagination">
	                	<li><a href="#" class="">0</a></li>
	                	<li><a href="#" class="">1</a></li>
	                	<li><a href="#" class="">2</a></li>
	                </ul>
	            </div>
	           
	            <script src="js/jquery.wmuSlider.js"></script> 
				<script>
					$('.example1').wmuSlider();         
				</script> 	
				<div class="shoes-grid-left"> 
	   		     	<div class=" con-sed-grid">
	   		     		<div class="elit-grid"> 
		   		     		<h4>Nouveauté</h4>
		   		     		<span>LAPINOU</span>
							<p>Rose et doux au toucher, Lapinou est le compagnon idéal pour les câlins.</p>
							<a class="now-get" href="#">Ajouter au panier</a>
						</div>
						<a href="article.php"><img class="img-responsive shoe-left" src="images/angelo.png" alt=" " /></a>
						<div class="clearfix"> </div>
	   		     	</div>
	   		     	<div class="con-sed-grid sed-left-top">
	   		     		<div class="elit-grid"> 
		   		     		<h4>Nouveauté</h4>
		   		     		<span>Photo volée</span>
							<p>Très rare, cette photo fera merveille dans son cadre doré, à côté de votre lit.</p>
							<a class="now-get" href="#">Ajouter au panier</a>
						</div>
						<a href="article.php"><img class="img-responsive shoe-left" src="images/rogeiro.png" alt=" " /></a>
						<div class="clearfix"> </div>
	   		     	</div>
	   		     </div>
	   		     <div class="products">
	   		     	<h5 class="latest-product">Tous nos produits</h5>	
	   		     	  <a class="view-all" href="liste_articles.php">Voir tout<span> </span></a> 		     
	   		     </div>
			</div>   
			<div class="sub-cate">
				<div class=" top-nav rsidebar span_1_of_left">
					<h3 class="cate">Catégories</h3>
					<ul class="menu">
						<li class="item1"><a href="#">Personnalités<img class="arrow-img" src="images/arrow1.png" alt=""/> </a>
							<ul class="cute">
								<li class="subitem"><a href="liste_articles.php">Angelo Rogeiro </a></li>
								<li class="subitem"><a href="liste_articles.php">Olivier Maccaud </a></li>
							</ul>
						</li>
						<li>
							<ul class="kid-menu">
								<li><a href="liste_articles.php">Peluches</a></li>
								<li><a href="liste_articles.php">Déguisements</a></li>
								<li><a href="liste_articles.php">Nourriture</a></li>
								<li><a href="liste_articles.php">Goodies</a></li>
								<li><a href="liste_articles.php">Beauté</a></li>
								<li><a href="liste_articles.php">Geek</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<!--script-->
				<script type="text/javascript">
					$(function() {
						var menu_ul = $('.menu > li > ul'),
						menu_a  = $('.menu > li > a');
					menu_ul.hide();
					menu_a.click(function(e) {
						e.preventDefault();
						if(!$(this).hasClass('active')) {
							menu_a.removeClass('active');
							menu_ul.filter(':visible').slideUp('normal');
							$(this).addClass('active').next().stop(true,true).slideDown('normal');
						} else {
							$(this).removeClass('active');
							$(this).next().stop(true,true).slideUp('normal');
						}
					});
			
				});
				</script>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="clearfix"> </div>
				
	<!--Footer-->
	<?php
	include('footer.php');
	?>
</body>
</html>