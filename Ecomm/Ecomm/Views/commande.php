﻿<?php
	session_start();
?>
<html>
	<head>
		<title>Commande | Rogeiro Store</title>
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.easydropdown.js"></script>
	</head>
	<body> 
	<!--Header-->
	<?php
		include('header.php');
	?>
	<!--Body--> 			         
	<div class="container"> 			         
		<div class="register">
			<div class="register-top-grid">
				<h3>État de la commande</h3>
			</div>
			<div class ="tableau-titre">Commande n° xxx</a></div>
			<div class ="tableau-liste">État :</a><span> test</span></div>
			<div class ="tableau-liste">Commandé le :</a><span> test</span></div>
			<div class ="tableau-liste">Liste des articles :</a><span> test</span></div>
			<a class="now-get" href="#">Ajouter au panier</a>
		</div>
		   <!--Catégories d'articles-->
			<div class="sub-cate">
				<div class="top-nav rsidebar span_1_of_left">
					<h3 class="cate">Catégories</h3>
					<ul class="menu">
						<li class="item1"><a href="#">Personnalités<img class="arrow-img" src="images/arrow1.png" alt=""/> </a>
							<ul class="cute">
								<li class="subitem"><a href="liste_articles.php">Angelo Rogeiro </a></li>
								<li class="subitem"><a href="liste_articles.php">Olivier Maccaud </a></li>
							</ul>
						</li>
						<li>
							<ul class="kid-menu">
								<li><a href="liste_articles.php">Peluches</a></li>
								<li><a href="liste_articles.php">Déguisements</a></li>
								<li><a href="liste_articles.php">Nourriture</a></li>
								<li><a href="liste_articles.php">Goodies</a></li>
								<li><a href="liste_articles.php">Beauté</a></li>
								<li><a href="liste_articles.php">Geek</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<!--initiate accordion-->
				<script type="text/javascript">
					$(function() {
						var menu_ul = $('.menu > li > ul'),
						menu_a  = $('.menu > li > a');
					menu_ul.hide();
					menu_a.click(function(e) {
						e.preventDefault();
						if(!$(this).hasClass('active')) {
							menu_a.removeClass('active');
							menu_ul.filter(':visible').slideUp('normal');
							$(this).addClass('active').next().stop(true,true).slideDown('normal');
						} else {
							$(this).removeClass('active');
							$(this).next().stop(true,true).slideUp('normal');
						}
					});
			
				});
				</script>
			</div>
		<div class="clearfix"> </div>
		</div>
	</div>
	
	<!--Footer-->
	<?php
		include('footer.php');
	?>
</body>
</html>