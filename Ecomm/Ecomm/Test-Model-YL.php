<?php
 
 Include 'Models/Context.php';
 echo "<h1> Hello World! voici les test de models</h1>";
 echo '<br />';
 echo '<br />';
 echo '<br />';
 
 ///////////////////////////
 //instancier un new context
 ///////////////////////////

 $test = new Context;


  
  
  /////////////////////
 // cr�ation des objets
 //////////////////////
 $user_exemple = new User(2,1,"L�che","Moi","01/01/2001","l�che-moi@gmail.com","password","suisse","lausanne","Sur rosset 24a");
 $user_exemple_Update = new User(2,1,"L�che","Moi","01/01/2001","LOL@gmail.com","password","suisse","lausanne","Sur rosset 24a");
 
 $article_exemple = new Article(2,"marc","tu pu du cul",23.90,true,"v�tement");
 $article_exemple_Update = new Article(2,"joel","tu pu du cul",23.90,true,"v�tement");
 
 $comment_exemple = new Comment(2,"voici mon commentaire",true,01/02/2010);
 $comment_exemple_Update = new Comment(2,"eh bah non",true,01/02/2010);
 
 $img_exemple = new Img(2,"www.facebook.com");
 $img_exemple_Update = new Img(2,"www.pitbull.com");
 
 $note_exemple = new Note(2,1);
 $note_exemple_Update = new Note(2,2);
 
 $order_exemple = new Order(2,"ab23fg00",01/02/2010);
 $order_exemple_Update = new Order(2,"ht64df3",01/02/2010);
 
 $orderstate_exemple = new OrderState(2,01/01/2010,2,2);
 $orderstate_exemple_Update = new OrderState(2,01/01/2010,3,3);
 
 $tag_exemple = new Tag(2,"monTag1");
 $tag_exemple_Update = new Tag(2,"monTag2");
 
 
 ////////////////
 // TEST du User
 ////////////////
 $test->AddUser($user_exemple);
 $user_test = $test->GetUsers();
 
 echo "Affichage des Users"."<br />";
 echo '<br />';
 echo $user_test['Role'] . " avec le nom " . $user_test['Name'];
 echo '<br />';
 echo $user_test['Email'];
 echo '<br />';
 echo $user_test['Birthdate'];
 echo "</br ></br >";
 
$user_test2 = $test->GetUser("l�che-moi@gmail.com");
 
 echo "Affichage d'un User"."<br />";
 echo '<br />';
 echo $user_test2['Role'] . " avec le nom " . $user_test2['Name'];
 echo '<br />';
 echo $user_test2['Email'];
 echo '<br />';
 echo $user_test2['Birthdate'];
 echo "</br ></br >";
 
 $test->UpdateUser($user_exemple_Update, 2); 
 $user_test3 = $test->GetUser("yolo@gmail.com");
 
 echo "Affichage d'un User updat�"."<br />";
 echo '<br />';
 echo $user_test3['Role'] . " avec le nom " . $user_test3['Name'];
 echo '<br />';
 echo $user_test3['Email'];
 echo '<br />';
 echo $user_test3['Birthdate'];
 echo "</br ></br >";
 
 $test->deleteUser(2);
 echo "l'utilisateur doit avoir �t� supprim�.'";
 echo '<br />';
 echo '<br />';
 
 //////////////////
 // TEST de Article
 //////////////////
 $test->AddArticles($article_exemple);
 $article_test = $test->GetArticles();
 
 echo "Affichage des Articles"."<br />";
 echo '<br />';
 echo $article_test['Name'];
 echo "</br ></br >";
 
$article_test2 = $test->GetArticle(2);
 
 echo "Affichage des Articles"."<br />";
 echo '<br />';
 echo $article_test2['Name'];
 echo "</br ></br >";
 
 $article_test3 = $test->GetArticlesByTag("monTag1");
 
 echo "Affichage des Articles"."<br />";
 echo '<br />';
 echo $article_test3['Name'];
 echo "</br ></br >";
 
 $article_test4 = $test->GetArticlesBytype("v�tement");
 
 echo "Affichage des Articles"."<br />";
 echo '<br />';
 echo $article_test4['Name'];
 echo "</br ></br >";
 
 $test->UpdateArticles($article_exemple_Update,2); 
$article_test5 = $test->GetArticle(2);
 
 echo "Affichage des Articles"."<br />";
 echo '<br />';
 echo $article_test5['Name'];
 echo "</br ></br >";
 
 $test->deleteArticles(2);
 echo "l'article doit avoir �t� supprim�.'";
 echo '<br />';
 echo '<br />';
 
 //////////////////
 // TEST de Comment
 //////////////////
 $test->AddComment($comment_exemple);
 $comment_test = $test->GetComments();
 
 echo "Affichage des comment"."<br />";
 echo '<br />';
 echo $comment_test['Contents'];
 echo "</br ></br >";
 
$comment_test2 = $test->GetCommentsByArticle(2);
 
 echo "Affichage des comment by article"."<br />";
 echo '<br />';
 echo $comment_test2['Contents'];
 echo "</br ></br >";
 
 $test->UpdateComment($comment_exemple_Update,2); 
 $comment_test3 = $test->GetComments();
 
 echo "Affichage des comment"."<br />";
 echo '<br />';
 echo $comment_test3['Contents'];
 echo "</br ></br >";
 
 $test->deleteComment(2);
 echo "le comment doit avoir �t� supprim�.'";
 echo '<br />';
 echo '<br />';
 
  //////////////////
 // TEST de Img
 //////////////////
 $test->AddImg($img_exemple);
 $img_test = $test->GetImgs();
 
 echo "Affichage des img"."<br />";
 echo '<br />';
 echo $img_test['URL'];
 echo "</br ></br >";
 
$img_test2 = $test->GetImgsByArticle(2);
 
 echo "Affichage des img by article"."<br />";
 echo '<br />';
 echo $img_test2['URL'];
 echo "</br ></br >";
 
 $test->UpdateImg($img_exemple_Update,2); 
 $img_test3 = $test->GetImgs();
 
 echo "Affichage des img"."<br />";
 echo '<br />';
 echo $img_test3['URL'];
 echo "</br ></br >";
 
 $test->deleteImg(2);
 echo "le img doit avoir �t� supprim�.'";
 echo '<br />';
 echo '<br />';
 
   //////////////////
 // TEST de Order
 //////////////////
 $test->AddOrder($order_exemple);
 $order_test = $test->GetOrders();
 
 echo "Affichage des orders"."<br />";
 echo '<br />';
 echo $order_test['Quantity'];
 echo "</br ></br >";
 
$order_test2 = $test->GetOrdersByUser(2);
 
 echo "Affichage des order by user"."<br />";
 echo '<br />';
 echo $order_test2['Quantity'];
 echo "</br ></br >";
 
 $test->UpdateOrder($order_exemple_Update,2); 
 $order_test3 = $test->GetOrders();
 
 echo "Affichage des order"."<br />";
 echo '<br />';
 echo $order_test3['Quantity'];
 echo "</br ></br >";
 
 $test->deleteOrder(2);
 echo "le order doit avoir �t� supprim�.'";
 echo '<br />';
 echo '<br />';
 
    //////////////////
 // TEST de tag
 //////////////////
 $test->AddTag($tag_exemple);
 $tag_test = $test->GetTags();
 
 echo "Affichage des tags"."<br />";
 echo '<br />';
 echo $tag_test['Name'];
 echo "</br ></br >";
 
$tag_test2 = $test->GetTagsByType(2);
 
 echo "Affichage des tag by type id"."<br />";
 echo '<br />';
 echo $tag_test2['Name'];
 echo "</br ></br >";
 
 $test->UpdateTag($tag_exemple_Update,2); 
 $tag_test3 = $test->GetTags();
 
 echo "Affichage des tag"."<br />";
 echo '<br />';
 echo $tag_test3['Name'];
 echo "</br ></br >";
 
 $test->deleteTag(2);
 echo "le tag doit avoir �t� supprim�.'";
 echo '<br />';
 echo '<br />';
?>
